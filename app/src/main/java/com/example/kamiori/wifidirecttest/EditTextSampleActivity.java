package com.example.kamiori.wifidirecttest;

import android.content.Intent;
import android.graphics.Color;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class EditTextSampleActivity extends AppCompatActivity {
    private WifiP2pInfo info;
    private ArrayAdapter<String> adapter;
    private Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text_sample);

        Button sendButton = (Button) findViewById(R.id.send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.edit_text);
//                Toast.makeText(getApplicationContext(), editText.getText(), Toast.LENGTH_SHORT).show();
                Log.d(MainActivity.TAG, "message:"+editText.getText().toString());
//                new SendMessageAsyncTask().execute(editText.getText().toString());
                new SendMessageAsyncTask().executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR,
                        editText.getText().toString());
            }
        });

        adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1){
            public View getView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                textView.setTextColor(Color.BLUE);

                return view;
            }
        };
        ListView listView = (ListView) findViewById(R.id.message_list_view);
        listView.setAdapter(adapter);

        Intent intent = getIntent();
        info = (WifiP2pInfo) intent.getExtras().get("connect_info");

//        new AcceptMessageAsyncTask().execute();
        new AcceptMessageAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        WifiP2pManager manager = SharedInstance.getManager();
        manager.removeGroup(SharedInstance.getChannel(), new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(MainActivity.TAG, "Disconnected");
            }

            @Override
            public void onFailure(int reason) {
                Log.d(MainActivity.TAG, "Disconnect failed : " + reason);
            }
        });
    }

    class AcceptMessageAsyncTask extends AsyncTask<String, String, String>{
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            if(info.groupFormed && info.isGroupOwner){
                ServerSocket serverSocket = null;
                try {
                    serverSocket = new ServerSocket(8988);
                    socket = serverSocket.accept();
                } catch (IOException e) {
                    Log.e(MainActivity.TAG, e.getMessage());
                }

            }else if(info.groupFormed){
                socket = new Socket();
                try {
                    socket.bind(null);
                    socket.connect(new InetSocketAddress(info.groupOwnerAddress.getHostAddress(), 8988), 5000);
                } catch (IOException e) {
                    Log.e(MainActivity.TAG, e.getMessage());
                }
            }
            //メッセージを受け取り続けるAsyncTask
            try {
                InputStream inputStream = socket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                //メッセージを受け付ける処理

                String str;
                while ((str = reader.readLine()) != null){
                    Log.d(MainActivity.TAG, "Message hash sent : " + str);
                    publishProgress(str);
//                    Toast.makeText(getApplicationContext(), "Message:"+str, Toast.LENGTH_SHORT).show();
                }

                Log.d(MainActivity.TAG, "Reading finished");
            } catch (IOException e) {
                Log.e(MainActivity.TAG, e.getMessage());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            for(String str : values){
                adapter.add(str);
            }
        }
    }

    class SendMessageAsyncTask extends AsyncTask<String, Void, String>{
        //メッセージを送るAsyncTask
        @Override
        protected String doInBackground(String... params) {
            if(socket == null){
                Log.e(MainActivity.TAG, "Socket is null");
                Toast.makeText(getApplicationContext(), "Please Wait", Toast.LENGTH_SHORT).show();
                return null;
            }
            try {
//                PrintWriter writer = new PrintWriter(new BufferedWriter(socket.getOutputStream()));
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                for(String message : params){
                    Log.d(MainActivity.TAG, "Sent : " + message);
                    writer.write(message+"\n");
                }
                writer.flush();
//                writer.close();
            } catch (IOException e) {
                Log.e(MainActivity.TAG, e.getMessage());
            }
            return null;
        }
    }
}
