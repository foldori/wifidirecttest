package com.example.kamiori.wifidirecttest;

import android.net.wifi.p2p.WifiP2pManager;

/**
 * Created by kamiori on 2016/09/19.
 */
public class SharedInstance {
    private static WifiP2pManager manager;
    private static WifiP2pManager.Channel channel;

    public SharedInstance(){}

    public static WifiP2pManager getManager() {
        return manager;
    }

    public static void setManager(WifiP2pManager manager) {
        SharedInstance.manager = manager;
    }


    public static WifiP2pManager.Channel getChannel() {
        return channel;
    }

    public static void setChannel(WifiP2pManager.Channel channel) {
        SharedInstance.channel = channel;
    }

}
